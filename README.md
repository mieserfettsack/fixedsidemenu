# fixedsidemenu

This is an easy to use "fixed sidebar menu" for TYPO3. If you need a menu for your mobile version, this extension gives
you exactly what you need. Version 3 will only work with browser that support [fetch](https://caniuse.com/?search=fetch).

## Difference to version 2

Version 2 of mdy/fixedsidemenu will work without JavaScript and on any currently used device or browser. This version
will print to entire menu as to every delivered page. Support for version 2 is still available. But this version will not
receive any new features.

## Use case and example

### Extension mdy/fixedsidemenu used in production environment

You can see this extension in action [here](https://www.theaterkompass.de). Just resize your browser to less than 640px
or switch to a mobile view. The menu opener will appear in the right top corner.

### Test extension mdy/fixedsidemenu in your environment

Just install the extension as described [here](#installation). After you have [included it](#include-extension) the
menu will automatically work and appear by default. You can decide whether it will be shown defined by media queries or
whatever you want.

You can use the constant editor the change its behaviour, color or what pages should be shown inside the menu. By
default it will show the entire page tree starting at the root page. You can define the pages via sitemap element, csv
or all together.

## Installation

This extension only works with TYPO3. Pick one of the following ways to install this extension to you TYPO3 page.

### Install via composer

`composer require mdy/fixedsidemenu`

### Install via TYPO3 Extension Manager

You can install mdy/fixedsidemenu via Extension Manager inside your TYPO3 installation. Just go to Extension Manager by
clicking on **Extensions**. Than choose **Get Extensions** and search for **fixedsidemenu**.

Click the **Import and Install** button.

### Install via release downloads

Go to https://extensions.typo3.org/extension/fixedsidemenu/#version-history and pick one of the releases available.
Download it and upload it to your TYPO3 via FTP, SSH or whatever.

## Include extension

### Include extension via backend
Go to one of your root pages and include the needed TypoScript files via **Template > Edit the whole template record >
Includes**. Choose **fixedsidemenu Basic Setup (fixedsidemenu)** and one of the available layouts like **fixedsidemenu:
Simple left or right w/b-menu (fixedsidemenu)**.

### Include extension via TypoScript

#### Include TypoScript Constants

```
@import 'EXT:fixedsidemenu/Configuration/TypoScript/constants.typoscript'
```

#### Include TypoScript Setup

```
@import 'EXT:fixedsidemenu/Configuration/TypoScript/setup.typoscript'
```

## Develop and test extension

[ddev](https://github.com/drud/ddev) is required! You can easily develop and test the extension using ddev on your local
machine. Git clone the extension and run the following command:

`ddev create`

This will create a fresh installation of TYPO3 11 with mdy/fixedsidemenu installed and ready to test. Point your browser to
`http://fixedsidemenu.ddev.site` when the script is done. Login into the backend here `http://fixedsidemenu.ddev.site/typo3`
 with user `admin` and the password  `adminadmin`.

### Assets build system

You can build all assets for development with this command:

`ddev assets`

#### Build before publish

If you are about to push a branch to repository please build assets for production like this:

`ddev build`

### Accepentce tests

You can run a test on your local machine with following command:

`ddev tests`

### Local linting

You can lint all files inside ddev environment by running:

`ddev lint`
