<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'fixedsidemenu',
    'description' => 'Easy to use off-canvas menu for TYPO3 (a menu which is fixed to a side). This extension adds a navigation that works an almost any browser, device and even with JavaScript disabled (version 2 only).',
    'category' => 'fe',
    'author' => 'Christian Stern',
    'author_email' => 'mail@christian-stern.eu',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'version' => '3.0.3',
    'constraints' => array(
        'depends' => array(
            'typo3' => '9.4.0-11.5.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);
