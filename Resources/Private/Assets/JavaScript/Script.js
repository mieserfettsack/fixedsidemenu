/*global JSONType, JSONAdditionalParams*/

function getJSONType() {
    if (typeof JSONType !== 'undefined') {
        return JSONType;
    } else {
        return 1337;
    }
}

function createElement(tagName, className) {
    let newElement = document.createElement(tagName);
    if (className) {
        newElement.className = className;
    }

    return newElement;
}

function getFirstElementByClassname(className) {
    return document.getElementsByClassName(className)[0];
}

function machtesMediaQuery(width) {
    const mediaQuery = window.matchMedia('(min-width: ' + width + ')');

    return mediaQuery.matches;
}

function createMenuBurger(jsonData) {
    if (jsonData.settings.fixedsidemenu.hmenu.button.disable === "0") {
        let
            fixedsidemenu__button__wrap = createElement("div", "fixedsidemenu__button__wrap fixedsidemenu__button__wrap--right"),
            fixedsidemenu__menu__wrap = getFirstElementByClassname("fixedsidemenu__menu__wrap")
        ;

        fixedsidemenu__button__wrap.innerText = jsonData.settings.fixedsidemenu.hmenu.button.string;

        fixedsidemenu__button__wrap.addEventListener("click", () => {
            fixedsidemenu__button__wrap.classList.toggle("fixedsidemenu__button__wrap--clicked");
            fixedsidemenu__menu__wrap.classList.toggle("fixedsidemenu__menu__wrap--clicked");
        });

        if (parseInt(jsonData.settings.fixedsidemenu.css.mediaqueries.enable) === 0) {
            fixedsidemenu__button__wrap.classList.add("fixedsidemenu__button__wrap--show");
        }

        document.body.append(fixedsidemenu__button__wrap);
    }
}

function createEachItem(title, link, doktype, children = false, depth) {
    let
        fixedsidemenu__menu__link__item = createElement("a", "fixedsidemenu__menu__link__item fixedsidemenu__menu__link__item--depth" + depth),
        fixedsidemenu__menu__link__wrap = createElement("div", "fixedsidemenu__menu__link__wrap fixedsidemenu__menu__link__wrap--depth" + depth),
        fixedsidemenu__menu__link__openchild,
        fixedsidemenu__menu__link__openchild__element,
        fixedsidemenu__menu__link__external,
        fragment = document.createDocumentFragment()
    ;

    fixedsidemenu__menu__link__item.href = link;
    fixedsidemenu__menu__link__item.innerText = title;

    // append a div that could hold an external link image
    if (doktype === 3) {
        fixedsidemenu__menu__link__external = createElement("div", "fixedsidemenu__menu__link__external");
        fixedsidemenu__menu__link__item.insertAdjacentElement("afterbegin", fixedsidemenu__menu__link__external);
    }

    fragment.append(fixedsidemenu__menu__link__item);

    if (children) {
        depth++;
        fixedsidemenu__menu__link__item.classList.add("fixedsidemenu__menu__link__item--children");
        fixedsidemenu__menu__link__openchild = createElement("div", "fixedsidemenu__menu__link__openchild");
        fixedsidemenu__menu__link__openchild__element = createElement("div", "fixedsidemenu__menu__link__openchild__element");
        fixedsidemenu__menu__link__openchild.append(fixedsidemenu__menu__link__openchild__element);
        fixedsidemenu__menu__link__item.append(fixedsidemenu__menu__link__openchild);
        fixedsidemenu__menu__link__item.addEventListener("click", function (events) {
            events.preventDefault();

            if (!events.target.classList.contains('fixedsidemenu__menu__link__item')) {
                this.nextElementSibling.classList.toggle("fixedsidemenu__menu__link__wrap--open");
                this.classList.toggle("fixedsidemenu__menu__link__item--active");
                this.querySelector(".fixedsidemenu__menu__link__openchild__element").classList.toggle("fixedsidemenu__menu__link__openchild__element--close");
            } else {
                window.open(this.href, "_self");
            }
        });

        children.forEach(item => {
            fixedsidemenu__menu__link__wrap
                .append(
                    createEachItem(
                        item.title,
                        item.link,
                        item.data.doktype,
                        item.children,
                        depth
                    )
                );
        });

        fixedsidemenu__menu__link__item.parentNode.insertBefore(fixedsidemenu__menu__link__wrap, fixedsidemenu__menu__link__item.nextSibling);
    }

    return fragment;
}

function createAdditionalHomepageLink(jsonData, fixedsidemenu__menu__wrap) {
    let homepagelink;
    if (jsonData.homepageLink) {
        homepagelink = createEachItem(jsonData.homepageLinkText, jsonData.homepageLink, 1, false, 0);
        fixedsidemenu__menu__wrap.append(homepagelink);
    }
}

function addAdditionalMenuContent(jsonData, fixedsidemenu__menu__wrap) {
    if (jsonData.afterbegin) {
        fixedsidemenu__menu__wrap.insertAdjacentHTML("afterbegin", jsonData.afterbegin);
    }

    if (jsonData.beforeend) {
        fixedsidemenu__menu__wrap.insertAdjacentHTML("beforeend", jsonData.beforeend);
    }
}

function addCSStoPage(jsonData) {
    const style = document.createElement('style');

    if (jsonData.CSS) {
        style.textContent += jsonData.CSS;
    }

    if (jsonData.settings.fixedsidemenu.css.fontfamily && parseInt(jsonData.settings.fixedsidemenu.css.disable) === 0) {
        const fontfamilyCSS = ".fixedsidemenu__menu__link__item { font-family:" + jsonData.settings.fixedsidemenu.css.fontfamily + ";}";
        style.textContent += fontfamilyCSS;
    }

    if (style.textContent) {
        document.head.append(style);
    }
}

function createMenu(jsonData) {
    let
        fixedsidemenu__menu__wrap = createElement("div", "fixedsidemenu__menu__wrap fixedsidemenu__menu__wrap--left"),
        depth = 0
    ;

    createAdditionalHomepageLink(jsonData, fixedsidemenu__menu__wrap);

    jsonData.fixedsidemenu.forEach(item => {
        fixedsidemenu__menu__wrap.append(
            createEachItem(
                item.title,
                item.link,
                item.data.doktype,
                item.children,
                depth
            )
        );
    });

    addAdditionalMenuContent(jsonData, fixedsidemenu__menu__wrap);
    addCSStoPage(jsonData);

    document.body.append(fixedsidemenu__menu__wrap);
    highlightCurrentPageInMenu();
    highlightActivePageTreeInMenu();
}

function setMenuStyles(jsonData) {
    let
        fixedsidemenu__menu__wrap = getFirstElementByClassname("fixedsidemenu__menu__wrap"),
        fixedsidemenu__button__wrap = getFirstElementByClassname("fixedsidemenu__button__wrap")
    ;

    if (jsonData.settings.fixedsidemenu.appearance.width) {
        fixedsidemenu__menu__wrap.style.width = jsonData.settings.fixedsidemenu.appearance.width;
    }

    if (jsonData.settings.fixedsidemenu.appearance.position.navigation.v === "Right") {
        fixedsidemenu__menu__wrap.classList.remove("fixedsidemenu__menu__wrap--left");
        fixedsidemenu__menu__wrap.classList.add("fixedsidemenu__menu__wrap--right");
    }

    if (jsonData.settings.fixedsidemenu.appearance.zindex) {
        fixedsidemenu__menu__wrap.style.zIndex = jsonData.settings.fixedsidemenu.appearance.zindex;
        fixedsidemenu__button__wrap.style.zIndex = jsonData.settings.fixedsidemenu.appearance.zindex;
    }

    if (
        jsonData.settings.fixedsidemenu.hmenu.button.disable === "0"
        && jsonData.settings.fixedsidemenu.appearance.position.button.v === "Left"
    ) {
        fixedsidemenu__button__wrap.classList.remove("fixedsidemenu__button__wrap--right");
        fixedsidemenu__button__wrap.classList.add("fixedsidemenu__button__wrap--left");
    }
}

function showElementBySelector (selector) {
    let element = document.querySelector(selector);
    if (element){
        element.style.display = 'block';
    }
}

function hideElementBySelector (selector) {
    let element = document.querySelector(selector);
    if (element) {
        element.style.display = 'none';
    }
}

function showMenuDependingOnWidth(jsonData) {
    if (parseInt(jsonData.settings.fixedsidemenu.css.mediaqueries.enable) === 1) {
        let fixedsidemenu__button__wrap = getFirstElementByClassname("fixedsidemenu__button__wrap");

        if (machtesMediaQuery(jsonData.settings.fixedsidemenu.css.mediaqueries.width)) {
            fixedsidemenu__button__wrap.classList.remove("fixedsidemenu__button__wrap--show");
            if (jsonData.settings.fixedsidemenu.appearance.hide.selector) {
                showElementBySelector(jsonData.settings.fixedsidemenu.appearance.hide.selector);
            }
        } else {
            fixedsidemenu__button__wrap.classList.add("fixedsidemenu__button__wrap--show");
            if (jsonData.settings.fixedsidemenu.appearance.hide.selector) {
                hideElementBySelector(jsonData.settings.fixedsidemenu.appearance.hide.selector);
            }
        }
    }
}

function createSkeleton(jsonData) {
    createMenu(jsonData);
    createMenuBurger(jsonData);
    setMenuStyles(jsonData);
    showMenuDependingOnWidth(jsonData);

    window.onresize = function () {
        showMenuDependingOnWidth(jsonData);
    };
}

function addJSONAdditionalParams() {
    if (typeof JSONAdditionalParams !== 'undefined') {
        return JSONAdditionalParams;
    } else {
        return "";
    }
}

async function getFixedsidemenuJSON() {
    fetch('/?type=' + getJSONType() + addJSONAdditionalParams())
        .then(response => {
            return response.json();
        })
        .then(jsonData => {
            createSkeleton(jsonData);
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}

function highlightCurrentPageInMenu() {
    let currentPath = window.location.pathname;
    let currentLink = document.querySelector(".fixedsidemenu__menu__wrap a[href='" + currentPath + "']");

    if (currentLink) {
        currentLink.classList.add("fixedsidemenu__menu__link__item--current");

        if (currentLink.classList.contains("fixedsidemenu__menu__link__item--children")) {
            currentLink.nextSibling.classList.add("fixedsidemenu__menu__link__wrap--open");
            currentLink.querySelector(".fixedsidemenu__menu__link__openchild__element").classList.toggle("fixedsidemenu__menu__link__openchild__element--close");
        }
    }
}

function highlightActivePageTreeInMenu() {
    let currentElement = document.querySelector(".fixedsidemenu__menu__link__item--current");

    if (currentElement) {
        let nextElement = currentElement.parentNode.previousSibling;

        while (
            typeof nextElement.classList !== "undefined"
            && nextElement.classList.contains('fixedsidemenu__menu__link__item')
        ) {
            nextElement.classList.add("fixedsidemenu__menu__link__item--active");
            nextElement.querySelector(".fixedsidemenu__menu__link__openchild__element").classList.toggle("fixedsidemenu__menu__link__openchild__element--close");
            nextElement.nextSibling.classList.add("fixedsidemenu__menu__link__wrap--open");
            nextElement = nextElement.parentNode.previousSibling;
        }
    }
}

if (window.fetch) {
    document.addEventListener("DOMContentLoaded", function () {
        getFixedsidemenuJSON();
    });
}
