const mix = require('laravel-mix');

mix.options({
    terser: {
        terserOptions : {
            ecma: 5,
            toplevel: true,
            mangle: true,
            compress: {
                sequences: true,
                dead_code: true,
                conditionals: true,
                booleans: true,
                unused: true,
                if_return: true,
                join_vars: true,
                drop_console: false
            }
        }
    },
    cssNano: {
        reduceIdents: true,
        normalizeWhitespace: true,
        mergeIdents: true,
        autoprefixer: true
    }
});

mix.scripts('Resources/Private/Assets/JavaScript/Script.js', 'Resources/Public/JavaScript/App.js');

mix.sass('Resources/Private/Assets/SCSS/Include.scss', 'Resources/Public/CSS/App.css');
