# Examples how to use this extension

You can change the extensions behaviour by settings constants or use the constant editor.

## Different types of items shown in the menu

### Default behaviour

This extension will try to use the next rootpage found in the page tree. It will than create a menu with the maximum depth
defined in {$plugin.mdy.fixedsidemenu.hmenu.depth}. Every page and shortcuts will be treated as item.

### Additional homepage link

A "homepage" - link as first item in the menu will not be created by default, except a separate page exist. If you need
an additional link to the homepage as first item use the following constants to achieve this:

```%typo3_typoscript
# 1 is the uid of the root page
plugin.mdy.fixedsidemenu.general.homepage.uid = 1
# If {$plugin.mdy.fixedsidemenu.general.homepage.title} is empty the page title will be used
plugin.mdy.fixedsidemenu.general.homepage.title = Title of the homepage
```
