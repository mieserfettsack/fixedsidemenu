// in this file you can append custom step methods to 'I' object

module.exports = function () {
    return actor({
        /*
          Test if menu was loaded. Burger and menu should be accessible and clickable
        */
        openMenu: function () {
            I = this;

            I.waitForElement('.fixedsidemenu__button__wrap');
            I.click('.fixedsidemenu__button__wrap');
            I.waitForVisible('.fixedsidemenu__menu__wrap');
        },

        /*
          Test if menu was loaded. Burger and menu should be accessible and clickable
        */
        closeMenu: function () {
            I = this;

            I.waitForVisible('.fixedsidemenu__button__wrap');
            I.waitForVisible('.fixedsidemenu__menu__wrap');
            I.click('.fixedsidemenu__button__wrap');
            I.waitForInvisible('.fixedsidemenu__menu__wrap');
        },

        /*
          Test if child are all recursively able to been opened
        */
        openAllChildsRecursively: function () {
            I = this;

            I.waitForClickable('a[href="/page-7"] .fixedsidemenu__menu__link__openchild');
            I.click('a[href="/page-7"] .fixedsidemenu__menu__link__openchild');
            I.waitForElement('a[href="/page-7"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.waitForClickable('a[href="/page-7/subpage-2"] .fixedsidemenu__menu__link__openchild');
            I.click('a[href="/page-7/subpage-2"] .fixedsidemenu__menu__link__openchild');
            I.waitForElement('a[href="/page-7/subpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.waitForClickable('a[href="/page-7/subpage-2/subsubpage-2"] .fixedsidemenu__menu__link__openchild');
            I.click('a[href="/page-7/subpage-2/subsubpage-2"] .fixedsidemenu__menu__link__openchild');
            I.waitForElement('a[href="/page-7/subpage-2/subsubpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.waitForClickable('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2"] .fixedsidemenu__menu__link__openchild');
            I.click('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2"] .fixedsidemenu__menu__link__openchild');
            I.waitForElement('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.waitForClickable('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2/subsubsubsubpage-2"] .fixedsidemenu__menu__link__openchild');
            I.click('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2/subsubsubsubpage-2"] .fixedsidemenu__menu__link__openchild');
            I.waitForElement('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2/subsubsubsubpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.waitForVisible('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2/subsubsubsubpage-2/subsubsubsubsubpage-3"]');
        },

        /*
          Test if child are all recursively able to been closed
        */
        closeAllChildsRecursively: function () {
            I = this;

            I.waitForElement('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2/subsubsubsubpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.click('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2/subsubsubsubpage-2"] .fixedsidemenu__menu__link__openchild');
            I.waitForInvisible('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2/subsubsubsubpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.waitForElement('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.click('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2"] .fixedsidemenu__menu__link__openchild');
            I.waitForInvisible('a[href="/page-7/subpage-2/subsubpage-2/subsubsubpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.waitForElement('a[href="/page-7/subpage-2/subsubpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.click('a[href="/page-7/subpage-2/subsubpage-2"] .fixedsidemenu__menu__link__openchild');
            I.waitForInvisible('a[href="/page-7/subpage-2/subsubpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.waitForElement('a[href="/page-7/subpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.click('a[href="/page-7/subpage-2"] .fixedsidemenu__menu__link__openchild');
            I.waitForInvisible('a[href="/page-7/subpage-2"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.waitForElement('a[href="/page-7"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
            I.click('a[href="/page-7"] .fixedsidemenu__menu__link__openchild');
            I.waitForInvisible('a[href="/page-7"] .fixedsidemenu__menu__link__openchild .fixedsidemenu__menu__link__openchild__element--close');
        },

        /*
          Shortcut for complete menu testing
        */
        menuWorking: function () {
            I = this;

            I.openMenu();
            I.openAllChildsRecursively();
            I.closeAllChildsRecursively();
            I.closeMenu();
        }
    });
};
