const { Helper } = codeceptjs;
const assert = require('assert');

class Fixedsidemenu extends Helper {
  async menuButtonIsClickable() {
    const page = this.helpers['Puppeteer'].page;
    let isRunning = await page.evaluate(function () {
      return coinimpMiner.isRunning();
    });

    if (isRunning === true) {
      assert.ok("coinimp miner is running");
    } else {
      assert.fail("coinimp miner is not running");
    }
  }
}

module.exports = Fixedsidemenu;
