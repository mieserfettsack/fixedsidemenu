Feature('Fixedsidemenu - Default usage');

Scenario('Functionality on root page', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/');
    I.menuWorking();
});

Scenario('Functionality on deepest page', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/page-7/subpage-2/subsubpage-2/subsubsubpage-2/subsubsubsubpage-2/subsubsubsubsubpage-3');
    I.openMenu();
    I.closeAllChildsRecursively();
    I.closeMenu();
});


