Feature('Fixedsidemenu - Settings');

Scenario('Change position of menu and opener', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=changePosition');
    I.menuWorking();
    I.waitForElement('.fixedsidemenu__button__wrap--left');
    I.waitForElement('.fixedsidemenu__menu__wrap--right');
});

Scenario('Additional content before and after menu', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=additionalContent');
    I.menuWorking();
    I.openMenu();
    I.waitForText("You can place content before the menu with mdy.fixedsidemenu.afterbegin");
    I.waitForText("You can place content after the menu with mdy.fixedsidemenu.beforeend");
});

Scenario('Additional homepage link and text', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=additionalHomepageLinkText');
    I.menuWorking();
    I.openMenu();
    I.waitForText("Homepage title (Set via constants)");
    I.waitForClickable('a[href="/"]');
    I.click('a[href="/"]');
});

Scenario('Create menu from sitemap element', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=menuBySitemap');
    I.menuWorking();
    I.openMenu();
    I.waitForText("Special page 1");
    I.waitForText("Special page 2");
    I.waitForText("Special page 3");
    I.waitForText("Page 1");
    I.waitForText("Page 7 (deep tree of subpages)");
});

Scenario('Create menu from CSV', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=menuByCSV');
    I.menuWorking();
    I.openMenu();
    I.waitForText("Special page 1");
    I.waitForText("Special page 2");
    I.waitForText("Special page 3");
    I.waitForText("Page 7 (deep tree of subpages)");
});

Scenario('Create menu from sitemap element and CSV', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=menuByCSVAndSitemap');
    I.menuWorking();
    I.openMenu();
    I.waitForText("Page 3");
    I.waitForText("Page 4");
    I.waitForText("Special page 1");
    I.waitForText("Special page 2");
    I.waitForText("Special page 3");
    I.waitForText("Page 1");
    I.waitForText("Page 7 (deep tree of subpages)");
});

Scenario('Mediaqueries activated', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=mediaqueries');
    I.waitForInvisible('.fixedsidemenu__button__wrap');
    I.waitForInvisible('.fixedsidemenu__menu__wrap');
    I.resizeWindow(799, 800);
    I.menuWorking();
});

Scenario('Menu opener disabled', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=openerdisabled');
    I.waitForInvisible('.fixedsidemenu__menu__wrap');
    I.resizeWindow(799, 800);
    I.waitForInvisible('.fixedsidemenu__menu__wrap');
});

Scenario('Change font-family of menu items text', async (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=changeFontFamily');
    I.menuWorking();
    I.seeCssPropertiesOnElements('.fixedsidemenu__menu__link__item', { 'font-family': "\"Lucida Console\", monospace"});
});

Scenario('Hide DOM element by selector', (I) => {
    I.amOnPage('http://fixedsidemenu.ddev.site/?tests=hideSelector');
    I.waitForVisible('#hideme');
    I.resizeWindow(799, 800);
    I.waitForInvisible('#hideme');
    I.menuWorking();
    I.resizeWindow(801, 800);
    I.waitForVisible('#hideme');
});
