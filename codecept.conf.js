const {setHeadlessWhen} = require('@codeceptjs/configure');

setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './Tests/CodeceptJS/Tests/*_test.js',
  output: './Tests/CodeceptJS/Output',
  helpers: {
    Fixedsidemenu: {
      require: './Tests/CodeceptJS/Helper/fixedsidemenu_helper.js',
    },
    Puppeteer: {
      "chrome": {
        "args": ["--no-sandbox", "--ignore-certificate-errors"]
      },
      url: 'http://fixedsidemenu.ddev.site/',
      show: false,
      windowSize: '1200x900',
      waitForTimeout: 4000,
      waitForAction: 250
    }
  },
  include: {
    I: './Tests/CodeceptJS/steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'html',
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
};
